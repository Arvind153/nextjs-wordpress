import { isEmpty } from 'lodash'

const Footer = ({footer, footerMenus}) => {
  
  if( isEmpty(footerMenus) ){
    return null
  }
  
  return (
    <footer className="bg-purple-700">
      <div className="container p-6 mx-auto">
        <div class="flex flex-wrap -mx-4 overflow-hidden text-white">
          <div class="my-3 px-3 w-full overflow-hidden sm:w-full md:w-1/2 lg:w-1/3">
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Porro, error atque, eos, earum tempora repudiandae dolorum rem nihil maxime aliquam nam? Nam accusamus culpa quos accusantium ex temporibus error a?
          </div>

          <div class="my-3 px-3 w-full overflow-hidden sm:w-full md:w-1/2 lg:w-1/3">
            Lorem ipsum, dolor sit amet consectetur adipisicing elit. Optio enim nobis aliquam commodi! Quaerat, facilis reprehenderit? Nesciunt explicabo voluptatibus ut assumenda inventore eos, culpa nihil aut, modi officia quas! Esse!
          </div>

          <div class="my-3 px-3 w-full overflow-hidden sm:w-full md:w-1/2 lg:w-1/3">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta voluptas temporibus ipsam! Aliquam corrupti, sed amet optio rerum quod, labore doloremque sequi voluptate, hic maxime quisquam id ad facilis accusamus.
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer