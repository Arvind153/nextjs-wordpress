import Link from 'next/Link'
import { useState } from 'react'

const Nav = ({ header ,headerMenus }) => {
  const [ isMenuVisable, setMenuVisibility ] = useState(false)
  console.log('header', header)
  return (
    <nav className="bg-purple-800 shadow ">
      <div className="container flex flex-wrap items-center justify-between p-6 mx-auto">
        <div className="flex items-center mr-6 text-white logo flex-no-shrink">
          <img className="mr-2" src={header.siteLogoUrl} alt={header.siteTitle} width="100" />
          <div className="flex flex-col justify-items-center">
          <span className="text-xl font-semibold tracking-tight">
            {header.siteTitle}
          </span>
          <span>{header.siteTagLine}</span>
          </div>
        </div>
        <div className="block lg:hidden">
          <button
            onClick={() => setMenuVisibility(!isMenuVisable)} 
            className="flex items-center px-3 py-2 border rounded text-teal-lighter border-teal-light hover:text-white hover:border-white">
            <svg className="w-3 h-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><title>Menu</title><path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/></svg>
          </button>
        </div>
        <div className={`${ isMenuVisable ? 'max-h-full' : 'h-0' } overflow-hidden lg:h-auto flex-grow block w-full lg:flex lg:items-center lg:w-auto`}>
        
        { headerMenus.length ? (
          <div className="text-sm lg:flex-grow">
              { headerMenus.map(menu => (
                <Link key={ menu.node.id } href={ menu.node.path }>
                  <a className="block mt-4 mr-4 lg:inline-block lg:mt-0 text-cyan-lighter hover:text-white">
                    { menu.node.label }
                  </a>
                </Link>
              )) }
          </div>
        ) : null }
      </div>
      </div>
    </nav>
  )
}

export default Nav