import Footer from "./footer"
import Header from "./header"
import Head from "next/head"

const Layout = ({data, children}) => {
  return (
  <div>
    <Head>
      <link rel="shortcut icon" href={data.header.favicon} type="image/x-icon"/>
      <title>{data.header.siteTitle}</title>
    </Head>
    <Header header={data.header} headerMenus={data.menus.headerMenus}/>
    <main className="container p-6 mx-auto">
      {children}
    </main>
    <Footer footer={data.footer} footerMenus={data.menus.footerMenus} />
  </div>
  )
}

export default Layout